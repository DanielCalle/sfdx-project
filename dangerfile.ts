import { warn, fail, danger } from 'danger';

const runTSLint = async () => {
    // Add a CHANGELOG entry for app changes
    const hasChangelog = danger.git.modified_files.includes("changelog.md");
    if (!hasChangelog) {
        warn("Please add a changelog entry for your changes.");
    }

    // Check MR Size
    let linesOfCode = await danger.git.linesOfCode();
    if (linesOfCode > 2000) {
        warn(`This merge request is definitely too big (${linesOfCode} lines changed), please split it into multiple merge requests`);
    } else if (linesOfCode > 500) {
        warn(`This merge request is quite big (${linesOfCode} lines changed), please consider splitting it into multiple merge requests.`);
    }

    // Always ensure we write MR description
    if (danger.gitlab.mr.description.length < 5) {
        fail('Please provide a proper merge request description.');
    }

    // Always ensure we use labels
    if (danger.gitlab.mr.labels.length === 0) {
        fail('Please add a label to this merge request.');
    }

    // Always ensure we assign someone
    if (danger.gitlab.mr.assignee === null) {
        fail("This merge request does not have any assignee yet. Setting an assignee clarifies who needs to take action on the merge request at any given time.");
    }

    // Always ensure we have a milestone
    if (danger.gitlab.mr.milestone === null) {
        fail('This merge request does not refer to an existing milestone.');
    }
};

runTSLint();